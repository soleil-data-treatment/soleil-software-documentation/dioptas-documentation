# Aide et ressources de Dioptas pour Synchrotron SOLEIL

## Résumé

- tomographie, réduction, diffraction. visualisation de spectres.

Dioptas is a Python-based program for on-the-fly data processing and exploration of two-dimensional X-ray diffraction area detector data. It is specifically designed for the large amount of data collected at XRD beamlines at synchrotrons.
- Open source

## Sources

- Code source:  https://github.com/Dioptas/Dioptas
- Documentation officielle:  https://dioptas.readthedocs.io/en/stable/

## Navigation rapide

| Tutoriaux |
| - |
| [Tutoriel d'installation officiel](https://github.com/Dioptas/Dioptas/releases) |
| [Tutoriaux officiels](https://dioptas.readthedocs.io/en/stable/#) |

## Installation

- Systèmes d'exploitation supportés: Windows
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: tiff,  cbf,  edf,  mccd
- en sortie: xy,  dat,  chi
- sur la Ruche
